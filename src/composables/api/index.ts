import type { AxiosResponse, Method } from 'axios'
import axios from 'axios'
import { ref } from 'vue'

export default function useApi() {
  const loading = ref(false)
  const error = ref()
  const BASE_URL = import.meta.env.VITE_API_BASE_URL

  async function fetchData<T>(url: string, method: Method = 'get', data?: any): Promise<T> {
    try {
      loading.value = true
      const response: AxiosResponse<T> = await axios.request<T>({
        url: `${BASE_URL}${url}`,
        method,
        data,
      })
      return response.data
    }
    catch (err: any) {
      error.value = err.response?.data || err
      throw err
    }
    finally {
      loading.value = !loading.value
    }
  }
  return {
    loading,
    error,
    fetchData,
  }
}
