/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",

  ],
  theme: {
    extend: {},
    fontFamily: {
      'Rubik': ['Rubik', 'sans-serif']
    },
    colors: {
     primary: "#75B758",
     indigo: colors.indigo,
      dark: colors.dark,
      black: colors.black,
      red: colors.red,
      white: colors.white
    },
    width: {
    }
  },

  plugins: [
    require("@tailwindcss/forms"), 
  ],
}

